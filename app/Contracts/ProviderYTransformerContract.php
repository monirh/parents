<?php

namespace App\Contracts;

interface ProviderYTransformerContract
{

    /**
     * Transform provider y interface.
     *
     * @param array $data
     * @return array
     */
    public function transformProviderY(array $data);
}
