<?php

namespace App\Http\Contracts;

interface UsersFilterRequestContract
{
    /**
     * Validate request.
     *
     * @param $request
     * @return bool
     */
    public function validate($request): bool;
}
