<?php

namespace App\Contracts;

interface ProviderXTransformerContract
{

    /**
     * Transform provider y interface.
     *
     * @param array $data
     * @return array
     */
    public function transformProviderX(array $data);
}
