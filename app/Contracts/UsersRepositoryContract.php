<?php

namespace App\Contracts;

interface UsersRepositoryContract
{
    /**
     * Read data from provider json file.
     *
     * @param string $path
     * @return array
     */
    public function getDataFromJsonDataProviders(string $path);

    /**
     * Filter users by provider name.
     *
     * @param array $users
     * @param array $filter
     * @return array
     */
    public function filterUsersByProvider(array $users, array $filter);

    /**
     * Filter users by status code.
     *
     * @param array $users
     * @param array $filterInputs
     * @return array
     */
    public function filterUsersByStatusCode(array $users, array $filterInputs);

    /**
     * Filter users by currency.
     *
     * @param array $users
     * @param array $filterInputs
     * @return array
     */
    public function filterUsersByCurrency(array $users, array $filterInputs);

    /**
     * Filter users by amount.
     *
     * @param array $users
     * @param array $filterInputs
     * @return array
     */
    public function filterUsersByAmount(array $users, array $filterInputs);
}
