<?php

namespace App\Http\Controllers;

use App\Contracts\UsersServiceContract;
use App\Traits\Functions;
use Illuminate\Http\Request;
use Mockery\Exception;

class UsersController extends Controller
{
    use Functions;

    private $userService;

    /**
     * UsersController constructor.
     * @param UsersServiceContract $userService
     */
    public function __construct(UsersServiceContract $userService)
    {
        $this->userService = $userService;
    }

    /**
     * List users with filter actions.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $filterInputs = $request->all();
            $users = $this->userService->mergeProviders($filterInputs);
            return $this->apiResponse('Get All Users List.', $users, 202);
        } catch (Exception $exception) {
            return $this->apiResponse('Failed To Get Users!', $exception, 404);
        }
    }
}
